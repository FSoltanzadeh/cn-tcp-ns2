#——-Event scheduler object creation——–#
set ns [new Simulator]

#———-creating trace objects—————-#

set nt [open tcp-tahoe.tr w]
$ns trace-all $nt
#———-creating nam objects—————-#

# set nf [open network.nam w]
# $ns namtrace-all $nf
#———-Setting color ID—————-#
$ns color 1 darkmagenta
$ns color 2 yellow
$ns color 3 blue
$ns color 4 green
$ns color 5 black

#———- Creating Network—————-#

set totalNodes 6

for {set i 1} {$i <= $totalNodes} {incr i} {
set node_($i) [$ns node]
}

set router1 3
set router2 4
set node1 1
set node2 2
set node5 5
set node6 6

# create random variables
set rng [new RNG]

$rng seed 0

#———- Creating Duplex Link—————-#
$ns duplex-link $node_($router1) $node_($node1) 100Mb 5ms DropTail
$ns duplex-link $node_($router1) $node_($node2) 100Mb [expr [$rng uniform 5 25]]ms DropTail

$ns duplex-link $node_($router2) $node_($node5) 100Mb 5ms DropTail
$ns duplex-link $node_($router2) $node_($node6) 100Mb [expr [$rng uniform 5 25]]ms DropTail

$ns duplex-link $node_($router1) $node_($router2) 100Kb 1ms DropTail

$ns duplex-link-op $node_($node1) $node_($router1) orient right-down
$ns duplex-link-op $node_($node2) $node_($router1) orient right-up

$ns duplex-link-op $node_($router2) $node_($node5) orient right-up
$ns duplex-link-op $node_($router2) $node_($node6) orient right-down

$ns duplex-link-op $node_($router1) $node_($router2) orient right

set tcp0 [new Agent/TCP]
set sink0 [new Agent/TCPSink]
$ns attach-agent $node_($node1) $tcp0
$ns attach-agent $node_($node5) $sink0
$ns connect $tcp0 $sink0
set ftp0 [new Application/FTP]
$ftp0 attach-agent $tcp0
$tcp0 set fid_ 1
$tcp0 set ttl_ 64


set tcp1 [new Agent/TCP]
set sink1 [new Agent/TCPSink]
$ns attach-agent $node_($node2) $tcp1
$ns attach-agent $node_($node6) $sink1
$ns connect $tcp1 $sink1
set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1
$tcp1 set fid_ 2
$tcp1 set ttl_ 64

$tcp0 attach $nt
$tcp0 tracevar cwnd_
$tcp0 tracevar ack_
$tcp0 tracevar rtt_

$tcp1 attach $nt
$tcp1 tracevar cwnd_
$tcp1 tracevar ack_
$tcp1 tracevar rtt_


#_____queue limit ____#
$ns queue-limit $node_($router1) $node_($router2) 10
$ns queue-limit $node_($router2) $node_($node5) 10
$ns queue-limit $node_($router2) $node_($node6) 10

#————Labelling—————-#

# $ns at 0.0 "$node_($router1) label Router"
# $ns at 0.0 "$node_($router2) label Router"  

# $ns at 0.0 "$node_($node1) label node1"
# $ns at 0.0 "$node_($node2) label node2"
# $ns at 0.0 "$node_($node5) label node5"
# $ns at 0.0 "$node_($node6) label node6"

$ns at 0.1 "$ftp0 start"
$ns at 0.1 "$ftp1 start"
$ns at 1000.1 "$ftp0 stop"
$ns at 1000.1 "$ftp1 stop"

$ns at 1000.2 "finish"

# $node_($node1) shape hexagon
# $node_($node2) shape hexagon
# $node_($node5) shape hexagon
# $node_($node6) shape hexagon

#———finish procedure——–#

proc finish {} {
global ns nf nt
$ns flush-trace
# close $nf
close $nt
puts "trace file created ..."
# exec nam network.nam &
exit 0
}

#Calling finish procedure
$ns run
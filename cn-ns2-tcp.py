import nstrace
import sys
import math
import importlib
from matplotlib import *
import matplotlib.pyplot as plt
import numpy as np

dictionary = {}

def function(fileName):
    global dictionary

    cwndF1 = [0]*1000
    rttF1 = [0]*1000
    dropF1 = [0]*1000
    goodputF1 = [0]*1000

    cwndF2 = [0]*1000
    rttF2 = [0]*1000
    dropF2 = [0]*1000
    goodputF2 = [0]*1000
    
    for i in range(10):
        tempgoodputF1 = [0]*1001
        tempgoodputF2 = [0]*1001
        tempCwndF1 = [-1]*1001
        tempCwndF2 = [-1]*1001
        tempRttF1 = [-1]*1001
        tempRttF2 = [-1]*1001
        nstrace.nsopen(fileName + "-" + str(i + 1) + ".tr")
        prevTF1 = 0
        prevAckF1 = 0
        newAckF1 = 0
        prevTF2 = 0
        prevAckF2 = 0
        newAckF2 = 0
        while not nstrace.isEOF():
            if nstrace.isEvent():
                data = nstrace.getEvent()
                if data[0] == 'd':
                    if data[7] == 1:
                        dropF1[math.floor(data[1])] += 1
                    else:
                        dropF2[math.floor(data[1])] += 1
            elif nstrace.isVar():
                data = nstrace.getVar()
                # print(data)

                if data[5] == "ack_":
                    if data[1] == 0:
                        t = math.floor(data[0])
                        if(prevTF1  != t):
                            prevAckF1 = newAckF1
                            prevTF1 = t
                        if t <= 0:
                            tempgoodputF1[t] = data[6]
                        else:
                            tempgoodputF1[t] = data[6] - prevAckF1
                        newAckF1 =data[6]
                        # print(tempgoodputF1[t], prevAckF1, newAckF1, prevTF1, t)
                    else:
                        t = math.floor(data[0])
                        if(prevTF2 != t):
                            prevAckF2 = newAckF2
                            prevTF2 = t
                        if t <= 0:
                            tempgoodputF2[t] = data[6]
                        else:
                            tempgoodputF2[t] = data[6] - prevAckF2
                        newAckF2 =data[6]
                elif data[5] == "cwnd_":
                    if data[1] == 0:
                        tempCwndF1[math.floor(data[0])] = data[6]
                    else:
                        tempCwndF2[math.floor(data[0])] = data[6]
                elif data[5] == "rtt_":
                    if data[1] == 0:
                        tempRttF1[math.floor(data[0])] = data[6]
                    else:
                        tempRttF2[math.floor(data[0])] = data[6]
            nstrace.skipline()
        importlib.reload(nstrace)
        lasttempCwndF1 = 0.0
        lasttempCwndF2 = 0.0
        lasttempRttF1 = 0.0
        lasttempRttF2 = 0.0
        for j in range(1000):
            if tempCwndF1[j] < 0:
                tempCwndF1[j] = lasttempCwndF1
            else:
                lasttempCwndF1 = tempCwndF1[j]

            if tempCwndF2[j] < 0:
                tempCwndF2[j] = lasttempCwndF2
            else:
                lasttempCwndF2 = tempCwndF2[j]

            if tempRttF1[j] < 0:
                tempRttF1[j] = lasttempRttF1
            else:
                lasttempRttF1 = tempRttF1[j]

            if tempRttF2[j] < 0:
                tempRttF2[j] = lasttempRttF2
            else:
                lasttempRttF2 = tempRttF2[j]
        
        for i in range(1000):
            goodputF1[i] += tempgoodputF1[i]
            goodputF2[i] += tempgoodputF2[i]
            cwndF1[i] += tempCwndF1[i]
            cwndF2[i] += tempCwndF2[i]
            rttF1[i] += tempRttF1[i]
            rttF2[i] += tempRttF2[i]
    dictionary[fileName + "-dropF1"] = [j/10 for j in dropF1]
    dictionary[fileName + "-goodputF1"] = [j/10 for j in goodputF1]
    dictionary[fileName + "-rttF1"] = [j*100 for j in rttF1]
    dictionary[fileName + "-cwndF1"] = [j/10 for j in cwndF1]
    dictionary[fileName + "-dropF2"] = [j/10 for j in dropF2]
    dictionary[fileName + "-goodputF2"] = [j/10 for j in goodputF2]
    dictionary[fileName + "-rttF2"] = [j*100 for j in rttF2]
    dictionary[fileName + "-cwndF2"] = [j/10 for j in cwndF2]

function("tcp-tahoe")
function("tcp-vegas")
function("tcp-newreno")

t = numpy.arange(0, 1000, 1).tolist()
plt.figure(1)
plt.ylabel("tahoe-rtt")
plt.plot(t, dictionary.get("tcp-tahoe-rttF1"), 'b')
plt.plot(t, dictionary.get("tcp-tahoe-rttF2"), 'r')

plt.figure(2)
plt.ylabel("vegas-rtt")
plt.plot(t, dictionary.get("tcp-vegas-rttF1"), 'b')
plt.plot(t, dictionary.get("tcp-vegas-rttF2"), 'r')

plt.figure(3)
plt.ylabel("newreno-rtt")
plt.plot(t, dictionary.get("tcp-newreno-rttF1"), 'b')
plt.plot(t, dictionary.get("tcp-newreno-rttF2"), 'r')

plt.show()

plt.figure(1)
plt.ylabel("tahoe-goodput")
plt.plot(t, dictionary.get("tcp-tahoe-goodputF1"), 'b')
plt.plot(t, dictionary.get("tcp-tahoe-goodputF2"), 'r')

plt.figure(2)
plt.ylabel("vegas-goodput")
plt.plot(t, dictionary.get("tcp-vegas-goodputF1"), 'b')
plt.plot(t, dictionary.get("tcp-vegas-goodputF2"), 'r')

plt.figure(3)
plt.ylabel("newreno-goodput")
plt.plot(t, dictionary.get("tcp-newreno-goodputF1"), 'b')
plt.plot(t, dictionary.get("tcp-newreno-goodputF2"), 'r')

plt.show()

plt.figure(1)
plt.ylabel("tahoe-cwnd")
plt.plot(t, dictionary.get("tcp-tahoe-cwndF1"), 'b')
plt.plot(t, dictionary.get("tcp-tahoe-cwndF2"), 'r')

plt.figure(2)
plt.ylabel("vegas-cwnd")
plt.plot(t, dictionary.get("tcp-vegas-cwndF1"), 'b')
plt.plot(t, dictionary.get("tcp-vegas-cwndF2"), 'r')

plt.figure(3)
plt.ylabel("newreno-cwnd")
plt.plot(t, dictionary.get("tcp-newreno-cwndF1"), 'b')
plt.plot(t, dictionary.get("tcp-newreno-cwndF2"), 'r')

plt.show()

plt.figure(1)
plt.ylabel("tahoe-drop")
plt.plot(t, dictionary.get("tcp-tahoe-dropF1"), 'b')
plt.plot(t, dictionary.get("tcp-tahoe-dropF2"), 'r')

plt.figure(2)
plt.ylabel("vegas-drop")
plt.plot(t, dictionary.get("tcp-vegas-dropF1"), 'b')
plt.plot(t, dictionary.get("tcp-vegas-dropF2"), 'r')

plt.figure(3)
plt.ylabel("newreno-drop")
plt.plot(t, dictionary.get("tcp-newreno-dropF1"), 'b')
plt.plot(t, dictionary.get("tcp-newreno-dropF2"), 'r')

plt.show()

# TODO:
#   1 - make 10 trace files * 3 congestion control type
#   2 - extract all trace files
#   3 - median calculation
#   4 - plot

#FLOW
# cwnd 
# rtt
# d
# 
